---
---

{{< container class="text-center text-small itinerary" >}}

![](https://apps.kde.org/app-icons/org.kde.itinerary.svg)

## KDE Itinerary

KDE Itinerary is a digital travel assistant that protects your privacy. It makes collecting all the information about your travel inside a single application easy and straightforward. KDE Itinerary is available for [Plasma Mobile](https://plasma-mobile.org/) and Android.

{{< /container >}}

{{< feature-container img="https://kde.org/for/travelers/itinerary-trip.png" >}}

## Store your reservations

Store all the information about your reservations in Itinerary. This includes QR-codes, check-in times, arrivial times, real-time delays, seat reservations, coach layout, and more.

Itinerary supports train, bus and flight bookings, as well as hotel, restaurant, event and rental car reservations. Traveling in a group? Not a problem, Itinerary supports multi-traveler bookings.

{{< /feature-container >}}

{{< feature-container img="https://kde.org/for/travelers/itinerary-ticket.png" reverse="true" >}}

## Local first

Itinerary automatically extracts booking data from various input formats. It's all performed locally on **your** device and your data is not sent to any remote servers.

This works best when using [KMail](https://kontact.kde.org/components/kmail/) to extract tickets from your email and then [KDE Connect](https://kdeconnect.kde.org) to transfer tickets to your phone. This also works great with [Nextcloud Mail](https://apps.nextcloud.com/apps/mail) and [DavDroid](https://f-droid.org/en/packages/at.bitfire.davdroid/) to sync your tickets from [Nextcloud](https://nextcloud.com/).

![KMail ticket extraction showing a train trip from Berlin to Tübingen](https://kde.org/for/travelers/kmail.png)

{{< /feature-container >}}

{{< feature-container img="https://kde.org/for/travelers/itinerary-connections.png" >}}

## Add your connections

Aside from finding reservations automatically in your email, Itinerary lets you add train trips manually to your journey, find alternative connections if your train is cancelled, or, for some providers, import your train trip directly from your reservation number.

{{< /feature-container >}}

{{< feature-container img="https://kde.org/for/travelers/itinerary-opening-hours.png" reverse="true" >}}

## Find your way

Powered by [OpenStreetMap](https://www.openstreetmap.org), the indoor map at train stations or airports can be a life saver. Use Itinerary to locate your platform is, and, if you have seat reservation and the train layout is available, it can even show you exactly which platform section is best for you.

![Train station map in KDE Itinerary, highlighting relevant platform sections.](https://www.volkerkrause.eu/assets/posts/139/kde-itinerary-platform-section-highlighting.jpg)

The indoor map also shows you which shops and resturantes are currently open, which elevator is broken (yet again!), where the toilets are and where the correct exit is.

{{< /feature-container >}}

{{< feature-container img="https://kde.org/for/travelers/itinerary-live-status.png" >}}

## Real time

It is rare that a train or bus departs or arrives on time. Itinerary keeps you updated when delays are announced.

On supported train and long distance buses, Itinerary will also use the onboard APIs to fetch the current live status of the vehicle and keep you updated on your current position and any announcements.

{{< /feature-container >}}
